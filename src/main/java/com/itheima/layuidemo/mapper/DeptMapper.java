package com.itheima.layuidemo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.*;

import com.itheima.layuidemo.pojo.Dept;
import org.springframework.context.annotation.Bean;

@Mapper
public interface DeptMapper {

	@Select("select * from dept where deptno=ifnull(#{arg2},deptno) or dname = ifnull(#{arg2},deptno) limit #{arg0},#{arg1}")//10
	List<Dept> showall(int begin, int end, String deptno);
	
	@Select("select count(*) from dept where deptno=ifnull(#{arg0},deptno) or dname = ifnull(#{arg0},deptno)")//11
	int getCount(String deptno);
	
	@Delete("delete from dept where deptno = #{deptno}")
	int deleteDeptno(int deptno);

	@Update("update dept set dname = #{dname},loc=#{loc} where deptno = #{deptno}")
	int update(Dept dept);

	@Insert("insert into dept(deptno,dname,loc) values(#{deptno},#{dname},#{loc})")
	int insert(Dept dept);
	
}
