package com.itheima.layuidemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.itheima.layuidemo.mapper.DeptMapper;
import com.itheima.layuidemo.pojo.Dept;

import javax.annotation.Resource;

@Service
public class DeptService {
    @Autowired
    DeptMapper deptMapper;

    public List<Dept> showAll(Integer page, Integer limit, String deptno) {
        int begin = (page - 1) * limit;
        deptno = ("".equals(deptno) ? null : deptno);
        return deptMapper.showall(begin, limit, deptno);
    }

    public int getcount(String deptno) {
        deptno = ("".equals(deptno) ? null : deptno);
        return deptMapper.getCount(deptno);
    }

    public int delete(int deptno) {
        return deptMapper.deleteDeptno(deptno);
    }

    public int update(Dept dept) {
        return deptMapper.update(dept);
    }

    public int insert(Dept dept) {
        return deptMapper.insert(dept);
    }

}
