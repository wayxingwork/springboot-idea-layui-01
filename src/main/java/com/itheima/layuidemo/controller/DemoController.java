package com.itheima.layuidemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DemoController {
    @GetMapping("/hello")
    public ModelAndView index(){
        ModelAndView mv = new ModelAndView();
        mv.addObject("testname","控制器传入的值");
        mv.setViewName("home/demo"); //跳转到使用thymeleaf 模板
        return mv;
    }
}
