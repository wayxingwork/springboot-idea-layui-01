package com.itheima.layuidemo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itheima.layuidemo.pojo.Dept;
import com.itheima.layuidemo.service.DeptService;

//springmvc + spring + mybatis 
@RestController
@CrossOrigin
public class DeptController {
	
	@Autowired
	DeptService ds;
	
	@GetMapping("/show")
	public Map<String,Object> showAll(Integer page,Integer limit,String deptno){
		Map<String,Object> map = new HashMap<>();
		List<Dept> list = ds.showAll(page,limit,deptno);
		map.put("deptno", deptno);//11 10
		map.put("data", list);//11 10
		map.put("code", 0);
		map.put("count", ds.getcount(deptno));
		map.put("msg", "跳转");
		System.out.println(map);
		return map;
	}
	
	@DeleteMapping("/del/{id}")
	public int deleteDept(@PathVariable Integer id) {
		return ds.delete(id);
	}
	
	@PostMapping("/update")
	public int update(Dept dept) {
		return ds.update(dept);
	}
	
	@PostMapping("/insert")
	public int insert(Dept dept) {
		return ds.insert(dept);
	}
}
