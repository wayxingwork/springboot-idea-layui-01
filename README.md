# springboot-idea-layui-01

# 简介
本例子演示使用LayUI访问数据库

基于 JDK1.8 + springboot 2.3.7

使用网络数据库进行演示

- 使用LayUI [layuidemo](http://localhost:8080/index.html)

![输入图片说明](https://images.gitee.com/uploads/images/2020/1111/214606_6de931b4_382074.png "屏幕截图.png")

- 使用thymeleaf模板  [hello](http://localhost:8080/hello)

![输入图片说明](https://images.gitee.com/uploads/images/2020/1111/214654_d31fc261_382074.png "屏幕截图.png")

### 说明
* LayUI作为静态资源，放在 resources/static 下
* 使用ajax方式获取数据 index.html
* thymeleaf是模板资源，放在 resources/templates 下
